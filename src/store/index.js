import Vue from 'vue'
import vuex from 'vuex'

Vue.use(vuex)

export default new vuex.Store({
    state:{
        //不够持久~
        // token:''
        //持久化处理
        token:localStorage.getItem('token')||''
        
    },
    mutations:{
        setToken(state,data){
            state.token = data
            //持久化处理
            localStorage.setItem('token',data)
        }
    }
})

    