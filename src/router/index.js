import Vue from 'vue'
import Router from 'vue-router'
import LogIn from '@/components/LogIn'
import Home from '@/components/Home'
import store from '@/store'
//二级路由
import Users from '@/components/users/Users'
import Roles from '@/components/rights/Roles'

Vue.use(Router)

const routes =  new Router({
  routes: [
    {
      path: '/login',
      name: 'login',
      component: LogIn,
      meta:{requiresAuth:true}
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      children:[
        {
          path:'users',
          name:'users',
          component:Users
        },
        {
          path:'rights',
          name:'rights',
          component:Roles
        }
      ]
    },
    //路由重定向
    {
      path:'/',
      redirect:'/login'
    }
  ]
})

routes.beforeEach((to,from,next)=>{
  if(!to.meta.requiresAuth){
    if (store.state.token) {
      next() 
    }else{
      next({
        path:'/login' 
      })
    }
  }else{
    next()
  }
})
 
export default routes